#include "lsGPG.h"

int main()
{
	lsGPG gpg;
	
	string str( "Hello, world!" );
	vector<char> data( str.begin(), str.end() );
	vector<char> output, output_dec;
	
	// encrypt some data.
	output = gpg.encrypt( data, "joe@blogs.com" );
	cout << string( output.begin(), output.end() ) << endl;
	
	// now decript it.
	output_dec = gpg.decrypt( output );
	cout << string( output_dec.begin(), output_dec.end() ) << endl;
	
	return 0;
}




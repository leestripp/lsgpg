#include "lsGPG.h"

int main()
{
	lsGPG gpg;
	
	// public key
	gpg.export_key( "joe@blogs.com" );
	
	// private key
	gpg.export_key( "joe@blogs.com", true );
	
	return 0;
}


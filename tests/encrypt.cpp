#include "lsGPG.h"

int main()
{
	lsGPG gpg;
	
	string str( "Hello, world!" );
	vector<char> data( str.begin(), str.end() );
	vector<char> output;
	
	// encrypt some data.
	output = gpg.encrypt( data, "joe@blogs.com" );
	cout << string( output.begin(), output.end() ) << endl;
	
	return 0;
}



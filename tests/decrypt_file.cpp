#include "lsGPG.h"

int main()
{
	lsGPG gpg;
	int err;
	
	// files
	string text_file( "test.gpg" );
	string img_file( "image.gpg" );
	
	// decrypt text file.
	cout << "decrypt text file : " << text_file << endl;
	err = gpg.decrypt_file( text_file, "decrypted.txt" );
	if( err )
	{
		cout << "ERROR: File decryption failed." << endl;
	} else
	{
		cout << "File decryption complete." << endl;
	}
	
	// decrypt image file.
	cout << "decrypt image file : " << img_file << endl;
	err = gpg.decrypt_file( img_file, "image_decrypted.png" );
	if( err )
	{
		cout << "ERROR: File decryption failed." << endl;
	} else
	{
		cout << "File decryption complete." << endl;
	}
	
	return 0;
}





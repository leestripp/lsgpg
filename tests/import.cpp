#include "lsGPG.h"

int main()
{
	// test public key
	string data = {
		"-----BEGIN PGP PUBLIC KEY BLOCK-----\n\n"
		"mDMEZtw9dRYJKwYBBAHaRw8BAQdAwcD8XdNQAcTtJW9YdF0D99i6CSYTrKS+k9sH\n"
		"m7/9l+K0IUpvZSBCbG9ncyAobHNHUEcpIDxqb2VAYmxvZ3MuY29tPoiTBBMWCgA7\n"
		"FiEEYj8F3dlnUEO5elegtHFwcptcqzsFAmbcPXUCGwMFCwkIBwICIgIGFQoJCAsC\n"
		"BBYCAwECHgcCF4AACgkQtHFwcptcqzsD+QD8DX6GhP4FFUITKOg0GSEtwdUcTxyL\n"
		"V7M9YPBSsZU0ajQA/011AmPcYRMAfOgWUSDbiA9GKj1cb+NsW5InsU63CBYIuDgE\n"
		"Ztw9dRIKKwYBBAGXVQEFAQEHQDhrQ/4o9aGlnG4ZdjuE1c13A2pjywk0HS5ZHPIR\n"
		"XJs3AwEIB4h4BBgWCgAgFiEEYj8F3dlnUEO5elegtHFwcptcqzsFAmbcPXUCGwwA\n"
		"CgkQtHFwcptcqzutQwD9GjmK++uTrrBlfeG2IBrkfKeJvA05SCJUtG2HsaztRg0B\n"
		"AIs/v6IY1zmKGMb5AZeFjXEQn1N5c4OvJvgzhOuGUiUO\n"
		"=aEEH\n"
		"-----END PGP PUBLIC KEY BLOCK-----\n"
	};
	vector<char> keys( data.begin(), data.end() );
	
	// lsGPG
	lsGPG gpg;
	
	// int import_key( const string& userid, const vector<char>& data );
	gpg.import_key( "joe@blogs.com", keys );
	
	return 0;
}



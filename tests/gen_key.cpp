#include "lsGPG.h"

int main()
{
	lsGPG gpg;
	
	// gen key : default (ed25519)
	gpg.gen_key( "Joe Blogs (lsGPG) <joe@blogs.com>", "default", true );
	
	return 0;
}

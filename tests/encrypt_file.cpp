#include "lsGPG.h"

int main()
{
	lsGPG gpg;
	int err;
	
	// files
	string text_file( "../data/test.txt" );
	string img_file( "../data/image.png" );
	
	// int encrypt_file( const string& in_file, const string& out_file, const string& userid, bool always_trust=false );
	// always_trust : use at your own risk. I Will remove this once I work out
	// how to modify trust on imported public keys.
	// encrypt text file.
	cout << "encrypt text file : " << text_file << endl;
	err = gpg.encrypt_file( text_file, "test.gpg", "joe@blogs.com", true );
	if( err )
	{
		cout << "ERROR: File encryption failed." << endl;
	} else
	{
		cout << "File encryption complete." << endl;
	}
	
	// encrypt image file.
	cout << "encrypt image file : " << img_file << endl;
	err = gpg.encrypt_file( img_file, "image.gpg", "joe@blogs.com", true );
	if( err )
	{
		cout << "ERROR: File encryption failed." << endl;
	} else
	{
		cout << "File encryption complete." << endl;
	}
	
	return 0;
}




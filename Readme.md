## lsGPG v0.4-Beta

# C++ Library
lsGPG is a wrapper for the gpgme C library.

# features
* generate keys.
* import keys.
* export public and private keys.
* encrypt/decrypt files
* encrypt/decrypt data.
* list all keys.

# TODO
* Find out how to modify trust. Currently using Always Trust flag to fix issue.
* testing.
* documentation.

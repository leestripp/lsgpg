#include "lsGPG.h"

lsGPG::lsGPG()
{
	progress_called = 0;
}

void lsGPG::list_keys()
{
	cout << "lsGPG : List all keys." << endl;
	cout << "----------------------" << endl;
	
	gpgme_ctx_t ctx;
	gpgme_error_t err;
	gpgme_key_t key;
	gpgme_protocol_t protocol = GPGME_PROTOCOL_OpenPGP;
	
	// init
	init_gpgme( protocol );
	// create context
	err = gpgme_new( &ctx );
	if(! err )
	{
		gpgme_set_protocol( ctx, protocol );
		
		err = gpgme_op_keylist_start( ctx, 0, 0 );
		while(! err )
		{
			err = gpgme_op_keylist_next( ctx, &key );
			if( err ) break;
			
			// display key info
			print_key( key );
			
			gpgme_key_release( key );
		}
		
		// cleanup
		gpgme_release( ctx );
	}
	
	if( gpg_err_code( err ) != GPG_ERR_EOF )
	{
		print_err( err );
	}
}

int lsGPG::gen_key( const string& userid, const string& algo, bool passphrase )
{
	gpgme_ctx_t ctx;
	gpgme_error_t err;
	gpgme_protocol_t protocol = GPGME_PROTOCOL_OpenPGP;
	uint flags = 0;
	
	// flags
	flags |= GPGME_CREATE_NOEXPIRE;
	if(! passphrase )
	{
		flags |= GPGME_CREATE_NOPASSWD;
	}
	
	// init
	init_gpgme( protocol );
	// create context
	err = gpgme_new( &ctx );
	if( err )
	{
		return print_err( err );
	}
	gpgme_set_protocol( ctx, protocol );
	
	// progress callback
	gpgme_set_progress_cb( ctx, progress, (void *)this );
	
	// generate key
	cout << "Generate key..." << endl;
	// gpgme_error_t gpgme_op_createkey (gpgme_ctx_t ctx, const char *userid, const char *algo, unsigned long reserved, unsigned long expires, gpgme_key_t extrakey, unsigned int flags);
	err = gpgme_op_createkey( ctx, userid.c_str(), algo.c_str(), 0, 0, NULL, flags );
	if( err )
	{
		// cleanup
		gpgme_release( ctx );
		return print_err( err );
	}
	if( progress_called ) printf ("\n");
	cout << "Complete..." << endl;
	
	// cleanup
	gpgme_release( ctx );
	
	return 0;
}

bool lsGPG::key_exists( const string& userid )
{
	if( userid.empty() ) return false;
	
	gpgme_ctx_t ctx;
	gpgme_error_t err;
	gpgme_protocol_t protocol = GPGME_PROTOCOL_OpenPGP;
	gpgme_key_t key;
	
	// init
	init_gpgme( protocol );
	// create context
	err = gpgme_new( &ctx );
	if( err )
	{
		print_err( err );
		return false;
	}
	gpgme_set_protocol( ctx, protocol );
	
	// get key
	err = gpgme_get_key( ctx, userid.c_str(), &key, 0 );
	if( err )
	{
		cerr << "ERROR: No key found." << endl;
		print_err( err );
		// cleanup
		gpgme_release( ctx );
		return false;
	}
	
	// cleanup
	gpgme_release( ctx );
	return true;
}

bool lsGPG::check_key( gpgme_ctx_t ctx, const string& userid )
{
	if( userid.empty() ) return false;
	
	gpgme_error_t err;
	gpgme_key_t key;
	
	// get key
	err = gpgme_get_key( ctx, userid.c_str(), &key, 0 );
	if( err )
	{
		cerr << "ERROR: No key found." << endl;
		print_err( err );
		return false;
	}
	return true;
}

int lsGPG::export_key( const string& userid, bool private_key )
{
	gpgme_error_t err;
	gpgme_ctx_t ctx;
	gpgme_data_t out;
	gpgme_protocol_t protocol = GPGME_PROTOCOL_OpenPGP;
	gpgme_export_mode_t mode = 0;
	
	// Mode
	if( private_key )
	{
		mode |= GPGME_EXPORT_MODE_SECRET;
	}
	
	// init
	init_gpgme( protocol );
	// create context
	err = gpgme_new( &ctx );
	if( err )
	{
		return print_err( err );
	}
	gpgme_set_protocol( ctx, protocol );
	gpgme_set_armor( ctx, 1 );
	
	err = gpgme_data_new( &out );
	if( err )
	{
		// cleanup
		gpgme_release( ctx );
		return print_err( err );
	}
	
	// Check key exists.
	if(! check_key( ctx, userid.c_str() ) )
	{
		cerr << "ERROR: No key found." << endl;
		// cleanup
		gpgme_data_release( out );
		gpgme_release( ctx );
		return -1;
	}
	
	// export.
	if( mode & GPGME_EXPORT_MODE_SECRET )
	{
		cout << "exporting private key." << endl;
	} else
	{
		cout << "exporting public key." << endl;
	}
	//  gpgme_error_t gpgme_op_export (gpgme_ctx_t ctx, const char *pattern, gpgme_export_mode_t mode, gpgme_data_t keydata)
	err = gpgme_op_export( ctx, userid.c_str(), mode, out );
	if( err )
	{
		// cleanup
		gpgme_data_release( out );
		gpgme_release( ctx );
		return print_err( err );
	}
	
	fflush( NULL );
	fputs( "Begin Result:\n", stdout );
	print_data( out );
	fputs( "End Result.\n", stdout );
	
	// Cleanup
	gpgme_data_release( out );
	gpgme_release( ctx );
	
	return 0;
}

int lsGPG::import_key( const string& userid, const vector<char>& data )
{
	gpgme_error_t err;
	gpgme_ctx_t ctx;
	gpgme_protocol_t protocol = GPGME_PROTOCOL_OpenPGP;
	
	// init
	init_gpgme( protocol );
	// create context
	err = gpgme_new( &ctx );
	if( err )
	{
		print_err( err );
		return -1;
	}
	gpgme_set_protocol( ctx, protocol );
	
	// convert data
	gpgme_data_t keydata;
	err = gpgme_data_new_from_mem( &keydata, data.data(), data.size(), 0 );
	if( err )
	{
		print_err( err );
		// cleanup
		gpgme_release( ctx );
		return -1;
	}
	
	// gpgme_error_t gpgme_op_import( gpgme_ctx_t ctx, gpgme_data_t keydata )
	// Import keys
	err = gpgme_op_import( ctx, keydata );
	if( err )
	{
		print_err( err );
		// cleanup
		gpgme_release( ctx );
		return -1;
	}
	
	// Check import result
	gpgme_import_result_t import_result = gpgme_op_import_result( ctx );
	cout << "Keys imported        : " << import_result->imported << endl;
	cout << "Imported secret keys : " << import_result->secret_imported << endl;
	cout << "Keys no user ID      : " << import_result->no_user_id << endl;
	cout << "Skipped v3 keys      : " << import_result->skipped_v3_keys << endl;
	cout << "Keys not imported    : " << import_result->not_imported << endl;
	
	// Check if single key was imported.
	if( import_result->imported != 1 )
	{
		// 0 or more then one key added, cant set policy.
		// cleanup
		gpgme_release( ctx );
		return 0;
	}
	
	// find key
	gpgme_key_t key;
	err = gpgme_op_keylist_start( ctx, userid.c_str(), 0 );
	while(! err )
	{
		err = gpgme_op_keylist_next( ctx, &key );
		if( err ) break;
		
		// display info
		print_key( key );
		
		// set TOFU policy. TODO: does this do anything?
		gpgme_error_t key_err = gpgme_op_tofu_policy( ctx, key, GPGME_TOFU_POLICY_AUTO );
		if( key_err )
		{
			print_err( key_err );
			gpgme_key_release( key );
			break;
		}
		
		gpgme_key_release( key );
	}
	
	// cleanup
	gpgme_release( ctx );
	
	return 0;
}


vector<char> lsGPG::encrypt( const vector<char>& data, const string& userid, bool always_trust )
{
	vector<char> result;
	
	gpgme_error_t err;
	gpgme_ctx_t ctx;
	gpgme_protocol_t protocol = GPGME_PROTOCOL_OpenPGP;
	gpgme_data_t in, out;
	// gpgme_encrypt_result_t encrypt_result;
	gpgme_key_t keys[2];
	int flags = 0;
	
	// flags
	if( always_trust )
	{
		flags |= (gpgme_encrypt_flags_t)GPGME_ENCRYPT_ALWAYS_TRUST;
	}
	
	// init
	init_gpgme( protocol );
	// create context
	err = gpgme_new( &ctx );
	if( err )
	{
		print_err( err );
		return result;
	}
	gpgme_set_protocol( ctx, protocol );
	
	// get key
	err = gpgme_get_key( ctx, userid.c_str(), &keys[0], 0 );
	if( err )
	{
		print_err( err );
		// cleanup
		gpgme_release( ctx );
		return result;
	}
	keys[1] = NULL;
	
	// load data
	err = gpgme_data_new_from_mem( &in, data.data(), data.size(), 0 );
	if( err )
	{
		fprintf( stderr, "ERROR: creating data buffer.\n" );
		// cleanup
	gpgme_key_unref( keys[0] );
		gpgme_release( ctx );
		return result;
	}
	
	// create output buffer.
	err = gpgme_data_new( &out );
	if( err )
	{
		print_err( err );
		// cleanup
		gpgme_data_release( in );
		gpgme_key_unref( keys[0] );
		gpgme_release( ctx );
		return result;
	}
	
	// encrypt
	// gpgme_error_t gpgme_op_encrypt( gpgme_ctx_t ctx, gpgme_key_t recp[], gpgme_encrypt_flags_t flags, gpgme_data_t plain, gpgme_data_t cipher )
	err = gpgme_op_encrypt( ctx, keys, (gpgme_encrypt_flags_t)flags, in, out );
	if( err )
	{
		fprintf( stderr, "ERROR: encrypting data failed\n" );
		// cleanup
		gpgme_data_release( out );
		gpgme_data_release( in );
		gpgme_key_unref( keys[0] );
		gpgme_release( ctx );
		return result;
	}
	
	// copy data.
	result = data_to_vector( out );
	
	// cleanup
	gpgme_data_release( out );
	gpgme_data_release( in );
	gpgme_key_unref( keys[0] );		// only 1 key used.
	gpgme_release( ctx );
	
	return result;
}

int lsGPG::encrypt_file( const string& in_file, const string& out_file, const string& userid, bool always_trust )
{
	// load data
	ifstream input( in_file, ios::binary );
	vector<char> in_buff( (istreambuf_iterator<char>(input)), istreambuf_iterator<char>() );
	vector<char> out_buff;
	if( in_buff.size() )
	{
		// encrypt data
		out_buff = encrypt( in_buff, userid, always_trust );
	} else
	{
		cerr << "ERROR: Failed to import file data." << endl;
		return 1;
	}
	
	// Save data
	ofstream output( out_file, ios::binary );
	if( out_buff.size() )
	{
		// save data
		cout << "Saving file : " << out_file << endl;
		output.write( out_buff.data(), out_buff.size() );
	} else
	{
		cerr << "ERROR: Failed to encrypt data." << endl;
		return 1;
	}
	
	return 0;
}

void lsGPG::encrypt_file( const string& out_file, const vector<char>& data, const string& userid, bool always_trust )
{
	vector<char> out_buff;
	if( data.size() )
	{
		// encrypt data
		out_buff = encrypt( data, userid, always_trust );
	} else
	{
		cerr << "ERROR: lsGPG::encrypt_file: Input data buffer empty." << endl;
		return;
	}
	
	// Save data
	ofstream output( out_file, ios::binary );
	if( out_buff.size() )
	{
		// save data
		cout << "Saving file : " << out_file << endl;
		output.write( out_buff.data(), out_buff.size() );
	} else
	{
		cerr << "ERROR: Failed to encrypt data." << endl;
	}
}


vector<char> lsGPG::decrypt( const vector<char>& data )
{
	vector<char> result;
	
	gpgme_error_t err;
	gpgme_ctx_t ctx;
	gpgme_protocol_t protocol = GPGME_PROTOCOL_OpenPGP;
	gpgme_data_t in, out;
	
	// init
	init_gpgme( protocol );
	// create context
	err = gpgme_new( &ctx );
	if( err )
	{
		print_err( err );
		return result;
	}
	gpgme_set_protocol( ctx, protocol );
	
	// load data
	err = gpgme_data_new_from_mem( &in, data.data(), data.size(), 0 );
	if( err )
	{
		fprintf( stderr, "ERROR: creating data buffer.\n" );
		// cleanup
		gpgme_release( ctx );
		return result;
	}
	
	err = gpgme_data_new( &out );
	if( err )
	{
		print_err( err );
		// cleanup
		gpgme_data_release( in );
		gpgme_release( ctx );
		return result;
	}
	
	// decrypt
	err = gpgme_op_decrypt( ctx, in, out );
	if( err )
	{
		fprintf( stderr, "ERROR: decrypting data failed\n" );
		// cleanup
		gpgme_data_release( out );
		gpgme_data_release( in );
		gpgme_release( ctx );
		return result;
	}
	
	// copy data.
	result = data_to_vector( out );
	
	// cleanup
	gpgme_data_release( out );
	gpgme_data_release( in );
	gpgme_release( ctx );
	
	return result;
}

int lsGPG::decrypt_file( const string& in_file, const string& out_file )
{
	// load data
	ifstream input( in_file, ios::binary );
	vector<char> in_buff( (istreambuf_iterator<char>(input)), istreambuf_iterator<char>() );
	vector<char> out_buff;
	if( in_buff.size() )
	{
		// decrypt data
		out_buff = decrypt( in_buff );
	} else
	{
		cerr << "ERROR: Failed to import file data." << endl;
		return 1;
	}
	
	// Save data
	ofstream output( out_file, ios::binary );
	if( out_buff.size() )
	{
		// save data
		cout << "Saving file : " << out_file << endl;
		output.write( out_buff.data(), out_buff.size() );
	} else
	{
		cerr << "ERROR: Failed to decrypt data." << endl;
		return 1;
	}
	
	return 0;
}

vector<char> lsGPG::decrypt_file( const string& in_file )
{
	vector<char> out_buff;
	
	// load data
	ifstream input( in_file, ios::binary );
	vector<char> in_buff( (istreambuf_iterator<char>(input)), istreambuf_iterator<char>() );
	if( in_buff.size() )
	{
		// decrypt data
		out_buff = decrypt( in_buff );
	} else
	{
		cerr << "ERROR: Failed to load file data." << endl;
	}
	return out_buff;
}

//*********************
// private

const char* lsGPG::nonnull( const char *s )
{
  return s ? s : "[none]";
}

int lsGPG::init_gpgme( gpgme_protocol_t proto )
{
	gpgme_error_t err;
	
	gpgme_check_version( NULL );
	setlocale( LC_ALL, "" );
	gpgme_set_locale( NULL, LC_CTYPE, setlocale(LC_CTYPE, NULL) );
	
	err = gpgme_engine_check_version( proto );
	if( err )
	{
		return print_err( err );
	}
	
	return 0;
}

vector<char> lsGPG::data_to_vector( gpgme_data_t dh )
{
	vector<char> buff;
	char ch;
	int ret = 0;
	
	ret = gpgme_data_seek( dh, 0, SEEK_SET );
	if( ret )
	{
		print_err( gpgme_err_code_from_errno( errno ) );
		return buff;
	}
	while( (ret = gpgme_data_read( dh, &ch, 1) ) > 0 )
	{
		buff.push_back( ch );
	}
	if( ret < 0 )
	{
		print_err( gpgme_err_code_from_errno( errno ) );
		return buff;
	}
	gpgme_data_seek( dh, 0, SEEK_SET );
	
	return buff;
}


string lsGPG::data_to_string( gpgme_data_t dh )
{
	vector<char> buff;
	char ch;
	int ret = 0;
	
	ret = gpgme_data_seek( dh, 0, SEEK_SET );
	if( ret )
	{
		print_err( gpgme_err_code_from_errno( errno ) );
		return "";
	}
	while( (ret = gpgme_data_read( dh, &ch, 1) ) > 0 )
	{
		buff.push_back( ch );
	}
	if( ret < 0 )
	{
		print_err( gpgme_err_code_from_errno( errno ) );
		return "";
	}
	gpgme_data_seek( dh, 0, SEEK_SET );
	
	return string( buff.begin(), buff.end() );
}


// callbacks

gpgme_error_t lsGPG::passphrase_cb( void *opaque, const char *uid_hint, const char *passphrase_info, int last_was_bad, int fd )
{
	int res;
	char pass[] = "test\n";
	int passlen = strlen( pass );
	int off = 0;
	
	// unused
	(void)opaque;
	(void)uid_hint;
	(void)passphrase_info;
	(void)last_was_bad;
	
	do
	{
		res = gpgme_io_write( fd, &pass[off], passlen - off );
		if (res > 0)
		off += res;
	} while( res > 0 && off != passlen );
	
	return off == passlen ? 0 : gpgme_error_from_errno (errno);
}

void lsGPG::progress( void *self, const char *what, int type, int current, int total )
{
	if(! strcmp (what, "primegen") && !current && !total
		&& (type == '.' || type == '+' || type == '!'
		|| type == '^' || type == '<' || type == '>') )
	{
		printf( "%c", type );
		fflush( stdout );
		static_cast<lsGPG *>(self)->progress_called = 1;
	} else
	{
		fprintf( stderr, "unknown progress `%s' %d %d %d\n", what, type, current, total );
		exit( 1 );
	}
}


// debug

void lsGPG::print_data( gpgme_data_t dh )
{
#define BUF_SIZE 512
	char buf[BUF_SIZE + 1];
	int ret;
	
	ret = gpgme_data_seek( dh, 0, SEEK_SET );
	if( ret )
	{
		print_err( gpgme_err_code_from_errno( errno ) );
		return;
	}
	while( (ret = gpgme_data_read( dh, buf, BUF_SIZE) ) > 0 )
	{
		fwrite( buf, ret, 1, stdout );
	}
	if( ret < 0 )
	{
		print_err( gpgme_err_code_from_errno( errno ) );
		return;
	}
	
	/*
	Reset read position to the beginning so that dh can be used as input
	for another operation after this method call. For example, dh is an
	output from encryption and also is used as an input for decryption.
	Otherwise GPG_ERR_NO_DATA is returned since this method moves the
	read position.
	*/
	ret = gpgme_data_seek( dh, 0, SEEK_SET );
}

int lsGPG::print_err( gpgme_error_t err )
{
	if( err )
	{
		fprintf( stderr, "%s: %s: %s\n", __FILE__, gpgme_strsource(err), gpgme_strerror(err) );
		return err;
	}
	
	return 0;
}

// print info

void lsGPG::print_key( gpgme_key_t key )
{
	cout << key->subkeys->keyid << " :";
	// features
	cout << " [";
	if( key->can_sign ) cout << "S";
	if( key->can_certify ) cout << "C";
	if( key->can_encrypt ) cout << "E";
	if( key->can_authenticate ) cout << "A";
	cout << "]";
	// trust
	cout << " Trust:" << key->owner_trust;
	// userid
	if( key->uids )
	{
		if( key->uids->name ) cout << " " << key->uids->name;
		if( key->uids->email ) cout << " <" << key->uids->email << ">";
	}
	cout << endl;
}


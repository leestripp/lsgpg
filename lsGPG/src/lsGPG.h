#ifndef LSGPG_H
#define LSGPG_H

// C
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <locale.h>
#include <string.h>
// gpgme
#include <gpgme.h>

#ifndef DIM
#define DIM(v)		     (sizeof(v)/sizeof((v)[0]))
#endif

// C++
#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>
#include <iterator>
#include <algorithm>

using namespace std;

class lsGPG
{
public:
	lsGPG();
	
	void list_keys();
	int gen_key( const string& userid, const string& algo, bool passphrase=false );
	int export_key( const string& userid, bool private_key=false );
	int import_key( const string& userid, const vector<char>& data );
	// utils
	bool key_exists( const string& userid );
	
	// encrypt
	vector<char> encrypt( const vector<char>& data, const string& userid, bool always_trust=false );
	int encrypt_file( const string& in_file, const string& out_file, const string& userid, bool always_trust=false );
	void encrypt_file( const string& out_file, const vector<char>& data, const string& userid, bool always_trust=false );
	
	// decrypt
	vector<char> decrypt( const vector<char>& data );
	int decrypt_file( const string& in_file, const string& out_file );
	vector<char> decrypt_file( const string& in_file );
	
private:
	const char* nonnull( const char *s );
	int init_gpgme( gpgme_protocol_t proto );
	string data_to_string( gpgme_data_t dh );
	vector<char> data_to_vector( gpgme_data_t dh );
	// utils
	bool check_key( gpgme_ctx_t ctx, const string& userid );
	
	// callbacks
	static void progress( void *self, const char *what, int type, int current, int total );
	static gpgme_error_t passphrase_cb( void *opaque, const char *uid_hint, const char *passphrase_info, int last_was_bad, int fd );
	
	// debug
	void print_data( gpgme_data_t dh );
	int print_err( gpgme_error_t err );
	void print_key( gpgme_key_t key );
	
	// vars
	int progress_called;
};

#endif // LSGPG_H